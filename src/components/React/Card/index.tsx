import React, { useState } from "react";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { addToCart } from "store/modules/cart/addToCart";
import { connect } from "react-redux";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";
import formatDate from "utils/utilsFunction/date-converter";
// import {
//   decreaseQuantity,
//   increaseQuantity,
//   deleteFromCart,
// } from "store/modules/cart/addToCart";
export type ProductData = {
  id: number;
  name: string;
  price: string;
  image: string;
  stock: number;
  createDate: string;
  category: string[];
};

export const Card = (props) => {
  const [count, setCount] = useState(0)
  const state = useSelector((state: RootStateOrAny) => state.cart)
  const { items } = props
  const dispatch = useDispatch();

  const addProduct = (product: ProductData) => {
    if (state.filter((object) => object.id === product.id).length === 0) {
      dispatch(addToCart({ ...product, quantity: 1 }));
    } else {
      dispatch(addToCart({ ...product }));
    }
  }
  function subQuantity() {
    setCount((prev) => prev - 1);
    };
  
    function addQuantity() {
      setCount((prev) => prev + 1);
    };

  // const addQuantity = (productId: number, quantity: number, stock: number) => {
  //   if (quantity < stock) {
  //     dispatch(increaseQuantity(productId));
  //   }
  // };
  // const subQuantity = (productId: number, quantity: number) => {
  //   if (quantity >= 1) {
  //     if (quantity > 1) {
  //       dispatch(decreaseQuantity(productId));
  //     } else {
  //       dispatch(deleteFromCart(productId));
  //     }
  //   }
  // };

  

  return (
    <>
      <div className="row">
        {items?.map((cart, index) => {

          return (
            <div className="col-lg-3 col-sm-4 col-6" key={index}>
              <div className="product-wrapper mb-3">
                <div className="product">
                  <img className="img-contain" src={process.env.REACT_APP_IMAGE_API_ENDPOINT + cart.image} />
                </div>
                <ul className="list product--details mt-3">
                  <li>
                    <div className="d-flex align-items-center justify-content-between">
                      {items?.category?.map((cat) => (
                        <span className="badge badge-info mr-1">{cat}</span>
                      ))}
                      <div className="input-group input-group-sm input--quantity_sm">
                        <div className="input-group-prepend">
                          <button
                            className="btn btn-outline-gray  border-0" 
                            id="decrease"
                            value="Decrease Value" onClick={subQuantity} >
                            <i className="ic-subtract" />
                          </button>
                        </div>
                        <input
                          className="form-control border-0"
                          type="number"
                          id="number"
                          defaultValue={count}
                        />
                        <div className="input-group-append">
                          <button
                            className="btn btn-outline-gray  border-0"
                            value="Increase Value" onClick={addQuantity} >
                            <i className="ic-add" />
                          </button>
                        </div>
                      </div>
                    </div>
                  </li>

                  <li>
                    <p className="font-14 mb-0">{cart.name}</p>
                  </li>
                  <li>
                    <div className="d-flex align-items-center justify-content-between">
                      <span className="amount">Rs. {getCommaSeperateNumber(cart.price.substring(1))}</span>
                      <li>
                        <p className="font-12 text-success">
                          Stocks left: {cart.stock}

                        </p>
                      </li>
                    </div>
                  </li>
                  <p> Released on: {formatDate(cart.createDate)}</p>
                  <li>

                    <button className="btn btn-sm btn-primary btn-block mt-3" onClick={() => addProduct(cart)} disabled={cart.stock === 0}>
                      Add to cart
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Card);
