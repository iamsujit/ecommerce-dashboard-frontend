import React from 'react'
import '../ErrorBoundary/ErrorBoundary.scss';



interface ErrorNotFoundProps {
}

function ErrorNotFound(props: ErrorNotFoundProps) {
    return (
        <main className="app-full-height app-flex-layout app-main-layout position-relative">
            <div className="app-flex-layout">
                <div className="error-boundary--container">
                    <div className="error-boundary--content">

                        <h1 className="error-boundary--header">404 | Not Found</h1>
                        <summary className="error-boundary--summary">
                            <h6>Oops, we can't seem to find the page you are looking for!</h6>
                        </summary>

                        {/* <div className="error-boundary--button">
                            <button className="btn btn-warning text-white" onClick={()=> onBackClick ?  onBackClick() : history.goBack()}>Go Back</button>
                        </div> */}
                    </div>
                </div>
            </div>
        </main>
    )
}

export default ErrorNotFound
