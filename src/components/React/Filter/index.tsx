import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { FaFilter } from 'react-icons/fa';
import { useFormik } from "formik";
import StyledSelect from "../StyledSelect/StyledSelect";

export const filterInitialValues = {
  category: "",
  priceMax: "",
  priceMin: "",
  date: "",
};

interface IProps {
  items?: any;
  setFilterData?: any;
  setFilteredList?: any;
  filteredList?: any;
  filterData?: any;
}

export const Filter = (props: IProps) => {
  const { items, setFilterData, filteredList, setFilteredList, filterData } = props;
  const [options, setOptions] = useState<OptionType[]>([])

  const getCategory = () => {
    let categoryOption = Array();
    items.map((item) => {
      item.category.map(cat => {
        categoryOption.push(cat)
      });
    })

    //Removing duplicate values
    categoryOption = categoryOption.filter((item, index, inputArray) => {
      return inputArray.indexOf(item) == index;
    });
    const optionVal = categoryOption.map((item) => ({
      label: item,
      value: item
    }))

    setOptions(optionVal);
  }

  useEffect(() => {
    getCategory()
  }, [items])


  const [modal, setModal] = useState<boolean>(false)
  const toggle = () => setModal(!modal)

  const { values, handleChange, handleSubmit, setFieldValue } = useFormik({
    enableReinitialize: true,
    initialValues: filterInitialValues,
    onSubmit: async (values: any, { resetForm }) => {
      const categoryValue = values?.category && values.category.map((cat) => {
        return cat.value
      });
      await setFilterData({
        date: values?.date,
        category: categoryValue,
        priceMax: values?.priceMax,
        priceMin: values?.priceMin,
      })
      toggle();
    }
  })


  useEffect(() => {
    if (filterData) {
      handleFilter(filterData);
    }
  }, [filterData])

  const handleDateFilter = (date: string) => {
    const filtered = items.filter(product => {
      const inputDate = new Date(date).toLocaleDateString()
      const productDate = new Date(product.createDate).toLocaleDateString()
      if (inputDate === productDate) {
        return product
      }
    })

    setFilteredList(filtered)
  }

  const handlePriceFilter = (minPrice = 0, maxPrice = 0) => {
    const filtered = items.filter(product => {
      const actualPrice = +product.price.replace("$", "")
      if (minPrice && maxPrice) {
        if (actualPrice >= minPrice && actualPrice <= maxPrice) {
          return product
        }
      }
      else if (maxPrice && actualPrice <= maxPrice) {
        return product
      }
      else if (minPrice && actualPrice <= minPrice) {
        return product
      }
    })
    setFilteredList(filtered)
  }

  const handleCategoryFilter = (category) => {
    const filtered = items.filter(product => {
      if (category.some(r => product.category.includes(r))) {
        return product
      }
    })
    setFilteredList(filtered)
  }

  const handleFilter = (data) => {
    const { date, priceMax, priceMin, category } = data;
    if (date) {
      handleDateFilter(date)
    }
    if (priceMin || priceMax) {
      handlePriceFilter(priceMin, priceMax)
    }
    if (category) {
      handleCategoryFilter(category)
    }

  }
  const handleFilterClose = (filterItem) => {
    const closeFilter = [values]?.filter(item => {
      if (filterItem !== Object.values(item)) {
        return item
      }
    })
  }

  return (
    <>
      <button className="btn btn-outline-primary btn-icon lft" onClick={toggle}>
        <FaFilter />
        Filter
      </button>

      <Modal isOpen={modal} toggle={toggle} modalClassName="modal-overlay" fade={false}>
        <ModalHeader toggle={toggle}>
          Filter
        </ModalHeader>
        <form onSubmit={(e) => {
          e.preventDefault();
          handleSubmit();
        }} className="d-flex flex-column flex-grow-1">
          <ModalBody>
            <div className="form-group">
              <label htmlFor="">Price</label>
              <div className="align-vertical">
                <input placeholder="Min" type="number" className="form-control" name="priceMin" value={values.priceMin} onChange={handleChange} />
                <span className="mx-2">-</span>
                <input placeholder="Max" type="number" className="form-control" name="priceMax" value={values.priceMax} onChange={handleChange} />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="">Date</label>
              <input type="date" className="form-control" name="date" value={values.date} onChange={handleChange} />
            </div>
            <div className="form-group" >
              <label htmlFor="">Category</label>
              <StyledSelect
                options={options || []}
                id="category"
                name="category"
                value={values.category}
                onChange={({ name, value }) => {
                  setFieldValue(name, value);
                }}
                multi
              />
            </div>


          </ModalBody>
          <ModalFooter>
            <button className="btn btn-outline-secondary" type="button" onClick={toggle}>Cancel</button>
            <button className="btn btn-success" type="submit">Apply</button>
          </ModalFooter>
        </form>

      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({});



const connector = connect(mapStateToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;


export default connector(Filter);
