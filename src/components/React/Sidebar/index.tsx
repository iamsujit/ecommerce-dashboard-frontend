import React from "react";
import { connect } from "react-redux";

export const index = (props) => {
  return (
    <>
      <aside className="sidebar">
        <a className="sidebar-brand" href="../index.html">
          <img src="assets/images/GreyJoy-Logo.png" alt="GreyJoy" />
        </a>
        <span className="ic-close toggler"  />
        <div className="sidebar-list-wrapper">
          <ul className="list sidebar-list scrollable">
            <li className="sidebar-item">
              <a className="sidebar-link active" href="index.html">
                Dashboard
              </a>
            </li>
            <li className="sidebar-item">
              <a
                className="sidebar-link hasSub"
                data-toggle="collapse"
                href="#Products"
                role="button"
                aria-expanded="false"
                aria-controls="Products"
              >
                Products
              </a>
              <ul className="list collapse pl-3 mt-1" id="Products">
                {" "}
                <li>
                  <a className="sidebar-link" href="product-list.html">
                    Products List
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="product-class-list.html">
                    Class
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="attribute-form.html">
                    Attribute
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="category-list.html">
                    Categories
                  </a>
                </li>
              </ul>
            </li>
            <li className="sidebar-item">
              <a className="sidebar-link" href="voucher-list.html">
                Tags
              </a>
            </li>
            <li className="sidebar-item">
              <a className="sidebar-link" href="banner.html">
                Banner
              </a>
            </li>
            <li className="sidebar-item">
              <a className="sidebar-link" href="stock-list.html">
                Stock Records
              </a>
            </li>
            <li className="sidebar-item">
              <a
                className="sidebar-link hasSub"
                data-toggle="collapse"
                href="#Offers"
                role="button"
                aria-expanded="false"
                aria-controls="Offers"
              >
                Offers
              </a>
              <ul className="list collapse pl-3 mt-1" id="Offers">
                {" "}
                <li>
                  <a className="sidebar-link" href="offer-list.html">
                    Offer List
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="voucher-list.html">
                    Voucher
                  </a>
                </li>
              </ul>
            </li>
            <li className="sidebar-item">
              <a className="sidebar-link" href="order-list.html">
                Order Lists
              </a>
            </li>
            <li className="sidebar-item">
              <a
                className="sidebar-link hasSub"
                data-toggle="collapse"
                href="#Shipping"
                role="button"
                aria-expanded="false"
                aria-controls="Shipping"
              >
                Shipping
              </a>
              <ul className="list collapse pl-3 mt-1" id="Shipping">
                {" "}
                <li>
                  <a className="sidebar-link" href="#">
                    Shipping List
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="#">
                    Methods
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="#">
                    Categories
                  </a>
                </li>
              </ul>
            </li>
            <li className="sidebar-item">
              <a className="sidebar-link" href="billing-list.html">
                Billing
              </a>
            </li>
            <li className="sidebar-item">
              <a className="sidebar-link" href="vendor-form.html">
                Vendors
              </a>
            </li>
            <li className="sidebar-item">
              <a
                className="sidebar-link hasSub"
                data-toggle="collapse"
                href="#Staff"
                role="button"
                aria-expanded="false"
                aria-controls="Staff"
              >
                Staff
              </a>
              <ul className="list collapse pl-3 mt-1" id="Staff">
                {" "}
                <li>
                  <a className="sidebar-link" href="#">
                    Staff List
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="#">
                    Staff Groups
                  </a>
                </li>
              </ul>
            </li>
            <li className="sidebar-item">
              <a className="sidebar-link" href="delivery-list.html">
                Delivery
              </a>
            </li>
            <li className="sidebar-item">
              <a className="sidebar-link" href="user-management-list.html">
                User Management
              </a>
            </li>
            <li className="sidebar-item">
              <a
                className="sidebar-link hasSub"
                data-toggle="collapse"
                href="#Alerts"
                role="button"
                aria-expanded="false"
                aria-controls="Alerts"
              >
                Alerts
              </a>
              <ul className="list collapse pl-3 mt-1" id="Alerts">
                {" "}
                <li>
                  <a className="sidebar-link" href="notification.html">
                    Notification
                  </a>
                </li>
              </ul>
            </li>
            <li className="sidebar-item">
              <a
                className="sidebar-link hasSub"
                data-toggle="collapse"
                href="#InnerPages"
                role="button"
                aria-expanded="false"
                aria-controls="InnerPages"
              >
                Inner Pages
              </a>
              <ul className="list collapse pl-3 mt-1" id="InnerPages">
                {" "}
                <li>
                  <a className="sidebar-link" href="contact.html">
                    Contact us
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="faq.html">
                    FAQ
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="privacy.html">
                    Privacy Policy
                  </a>
                </li>
                <li>
                  <a className="sidebar-link" href="terms.html">
                    Terms &amp; Condition
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </aside>
    </>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(index);
