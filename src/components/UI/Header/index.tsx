import React from "react";
import { connect } from "react-redux";

export const Header = (props) => {
  return (
    <>
      <header className="header">
        <div className="header-wrapper">
        
          <ul className="list list-inline nav-list">
            <li className="nav-item expandSearch toggleSearch">
              <a
                className="nav-link btn btn-icon-only"
                href="javascript:void(0)"
              >
                <i className="ic-search" />
              </a>
            </li>
            <li className="nav-item list-separator">
              <a className="nav-link text-white" href="javascript:void(0)">
                Visit Shop
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link btn btn-icon-only"
                href="javascript:void(0)"
              >
                <i className="ic-information-filled" />
              </a>
            </li>
            <li className="nav-item dropdown-notification">
              <a
                className="nav-link btn btn-icon-only dropdown-toggle"
                href="javascript:void(0)"
                id="notificationDrop"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <i className="ic-notification-filled" />
                <small className="notification badge-count">0</small>
              </a>
              <div
                className="dropdown-menu dropdown-menu-right"
                aria-labelledby="notificationDrop"
              >
                <div className="heading">
                  <p className="font-weight-semiBold">Notifications</p>
                  <a className="text-info" href="#">
                    See All
                  </a>
                </div>
                <ul className="list scrollable">
                  <li className="dropdown-item">
                    <a href="#">
                      <p>New order received. Click here to view the detail.</p>
                      <ul className="list list-inline list-dot">
                        {" "}
                        <li>2077/08/11</li>
                      </ul>
                    </a>
                  </li>
                  <li className="dropdown-item">
                    <a href="#">
                      <p>
                        Office audit plan for office Chikitsa Bigyan has been
                        recommended for revision to update checklist.
                      </p>
                      <ul className="list list-inline list-dot">
                        {" "}
                        <li>Sabin Ghimire </li>
                        <li>2077/08/11</li>
                      </ul>
                    </a>
                  </li>
                  <li className="dropdown-item">
                    <a href="#">
                      <p>New order received. Click here to view the detail.</p>
                      <ul className="list list-inline list-dot">
                        <li>2077/08/11</li>
                      </ul>
                    </a>
                  </li>
                  <li className="dropdown-item">
                    <a href="#">
                      <p>New order received. Click here to view the detail.</p>
                      <ul className="list list-inline list-dot">
                        <li>2077/08/11</li>
                      </ul>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
            <li className="nav-item">
              <div className="dropdown">
                <a
                  className="nav-link avatar dropdown-toggle"
                  href="javascript:void(0)"
                  id="profileDrop"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <img
                    src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                    alt="Avatar"
                  />
                </a>
                <div
                  className="dropdown-menu dropdown-menu-right"
                  aria-labelledby="profileDrop"
                >
                  <div className="dropdown-item">
                    <h4 className="mb-0">Admin</h4>
                    <p className="text-muted mb-0">admin@example.com</p>
                  </div>
                  <div className="dropdown-divider" />
                  <a className="dropdown-item" href="#">
                    Logout
                  </a>
                </div>
              </div>
            </li>
            <li className="nav-item"> Rabindra K.C</li>
          </ul>
        </div>
      </header>
    </>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
