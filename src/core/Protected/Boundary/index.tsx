import React, { ReactElement, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux';
import PrivateRoute from '../../../routes/PrivateRoute/PrivateRoute';
import { RootState } from '../../../store/root-reducer';


interface Props extends PropsFromRedux {
    children: CustomRoute[];
}

function Boundary(props: Props): ReactElement {
    const { children } = props;

    return (
        <PrivateRoute appRoutes={children} />
    )
}

const mapStateToProps = (state: RootState) => ({
})

const mapDispatchToProps = {
}

const connector = connect(mapStateToProps, mapDispatchToProps)
type PropsFromRedux = ConnectedProps<typeof connector>;

export default connector(Boundary)