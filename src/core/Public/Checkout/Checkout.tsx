import React from "react";
import { Header } from "../Dashboard/Header/Header";
import ProducImage from "assets/images/display-1.jpg"
import { Link } from "react-router-dom";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";
import { deleteFromCart } from "store/modules/cart/addToCart";

export default function Checkout() {
  const state = useSelector((state: RootStateOrAny) => state.cart)
 

 

 const totalAmount = getCommaSeperateNumber(
    state
      .map((item) => Number(item.price.substring(1)) * item.quantity)
      .reduce((acc, current) => {
        return acc + current;
      }, 0)
  );
  
  const dispatch = useDispatch();
  const deleteItem = (productId: number) => {
    dispatch(deleteFromCart(productId));
  };

  return (
    <>
      <div className="container">
        <div className="section--header mt-4 mb-3">
          <h2 className="mb-0">Your Cart</h2>
        </div>
      </div>
      {
        state ?
          (
            <>
              {state.map((cart, index) => (

                <div className="row">
                  <div className="col-lg-8">
                    <section>
                      <div className="d-flex justify-content-between mb-2">
                        <span className="font-14 text-gray-500"> {cart.item}</span>
                        <Link to="" className="text-info font-14">
                          Shop More
                        </Link>
                      </div>
                      <div className="product--cart">
                        <ul className="list"> 
                          <li><div className="d-flex">
                          {index + 1} 
                            <div className="product mr-3">
                              <img className="img-container" src={process.env.REACT_APP_IMAGE_API_ENDPOINT + cart.image} />
                            </div>
                            <div>
                              <p className="mb-0">
                           
                          {cart.name}</p>
                              <div className="d-flex mt-2 font-14">
                                <div>
                            <span className="text-gray-500">Items ordered :</span>  {cart?.quantity}
                          </div>
                                
                              </div>
                            </div>
                          </div>
                            <div>
                              <div className="mb-3">
                                <form>
                                
                                </form>
                              </div>
                              <ul className="list list-inline list-separator font-14">
                                <li>
                                <button 
                                        onClick={() => deleteItem(cart.id)}
                                        className="btn btn-sm btn-danger btn-block mt-1" >Remove</button>
                                </li>
                                <li>
                                  <a className="text-info" href="#">Add to wishlist</a>
                                </li>
                              </ul>
                            </div>
                            <div>
                              <p className="mb-0">Rs.&nbsp;{getCommaSeperateNumber(cart?.price.substring(1))} <br /> (per item)</p>
                            </div>
                          </li>

                        </ul>
                      </div>
                    </section>
                  </div>

                
                  <p></p>
                </div>
              ))}
                <div className="col-lg-8" >
                    <aside className="product--summary mt-lg-4" >
                      <div className="card">
                        <div className="card-body">
                          <h3 className="head">Order Summary</h3>
                          
                          <table className="table table-borderless">
                            <tbody>
                              <tr>
                                <td>Total:</td>
                                <td className="text-right font-weight-medium">Rs. {totalAmount}</td>
                              </tr>
                            </tbody>
                          </table>
                          <Link to="/checkout-form">
                            <a className="btn btn-success btn-block" href="checkout-shipping.html">Checkout</a></Link>
                        </div>
                      </div>
                    </aside>
                  </div>
            </>
          )
          : (
            <section className="section--spacer">
              <div className="message message--primary">
                <div className="icon mb-3"><i className="ic-shopping-cart" /></div>
                <p className="mb-4 text-gray-500">There is no item in your cart</p><a className="btn btn-primary" href="index.html">Continue Shopping</a>
              </div>
            </section>
          )}
    </>
  );
};
