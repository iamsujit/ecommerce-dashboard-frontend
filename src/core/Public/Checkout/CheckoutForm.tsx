import React, { useEffect, useState } from "react";
import FormikValidationError from "../../../components/React/FormikValidationError/FormikValidationError";
import LoadingButton from "../../../components/React/LoadingButton/LoadingButton";
import { checkoutValidation } from "./schema";
import { Formik } from "formik";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import ProducImage from "assets/images/display-1.jpg";
import { emptyCart } from "store/modules/cart/addToCart";

const initialValues = {
  name: "",
  billing_address: "",
  delivery_address: "",
  contact: "",
  current_date: "" as any,
};

export default function CheckoutForm() {
  const dispatch = useDispatch()
  const [isloading, setIsloading] = useState(false);
  const [cartItems, setCartItems] = useState<any>([]);


  const resData =  useSelector((state: RootStateOrAny) => state.cart);

  const initialSetter = async () => {
    try {
      setCartItems(resData)
      console.log("ss",resData)
    } catch (error) {
      alert('Error')
    }
  }


  const handleSumit = async (data: typeof initialValues, resetForm) => {
    try {
      const responseData = {
        name: data.name,
        billing_address: data.billing_address,
        delivery_address: data.delivery_address,
        contact: data.contact,
        current_date: data.current_date,
      };
      alert(JSON.stringify(responseData));
      resetForm();
      setCartItems([])
      dispatch(emptyCart())
    } catch (error) {
      alert("Something went wrong");
    }
  };

  useEffect(() => {
    initialSetter()
  },[])

  return (
    <>
      <div className="section-container bg-white">
        <div className="container-fluid">
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={checkoutValidation}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              setSubmitting(false);
              handleSumit(values, resetForm);
            }}
          >
            {({
              values,
              touched,
              errors,
              handleChange,
              setFieldValue,
              handleSubmit,
            }) => (
              <form onSubmit={handleSubmit}>
                <h6 className="form-title">Checkout: All Cart Items</h6>
                {cartItems.map((item) => (
                  <div className="product--cart">
                    <ul className="list">
                      <li>
                        <div className="d-flex">
                          <div className="product mr-3">
                            <img className="img-contain" src={process.env.REACT_APP_IMAGE_API_ENDPOINT + item?.image} />
                          </div>
                          <div>
                            <p className="mb-0">{item?.name}</p>
                            <div className="d-flex mt-2 font-14">
                            </div>
                          </div>
                        </div>
                        
                        <div>
                          <p className="mb-0">{item?.price}</p>
                         
                        </div>
                      </li>
                    </ul>
                  </div>
                ))}

                <div className="form-row">
                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="">Name</label>
                      <input
                        type="text"
                        className="form-control"
                        value={values.name}
                        name="name"
                        onChange={handleChange}
                      />

                      <FormikValidationError
                        name="name"
                        errors={errors}
                        touched={touched}
                      />
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="">Billing Address</label>
                      <input
                        type="text"
                        className="form-control"
                        value={values.billing_address}
                        name="billing_address"
                        onChange={handleChange}
                      />

                      <FormikValidationError
                        name="billing_address"
                        errors={errors}
                        touched={touched}
                      />
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="">Delivery Address</label>
                      <input
                        type="text"
                        className="form-control"
                        value={values.delivery_address}
                        name="delivery_address"
                        onChange={handleChange}
                      />

                      <FormikValidationError
                        name="delivery_address"
                        errors={errors}
                        touched={touched}
                      />
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="">Contact</label>
                      <input
                        type="text"
                        className="form-control"
                        value={values.contact}
                        name="contact"
                        onChange={handleChange}
                      />

                      <FormikValidationError
                        name="contact"
                        errors={errors}
                        touched={touched}
                      />
                    </div>
                  </div>

                  <div className="col-md-4">
                    <label>Date</label>
                    <input
                      type="date"
                      className="form-control"
                      value={values.current_date}
                      name="current_date"
                      onChange={handleChange}
                    />
                    <FormikValidationError
                      name="current_date"
                      errors={errors}
                      touched={touched}
                    />
                  </div>
                </div>
                <div className="text-right mt-3">
                  <button
                    type="button"
                    // onClick={handleBackToUserList}
                    className="btn btn-outline-secondary text-gray-40"
                  >
                    Cancel
                  </button>
                  <LoadingButton
                    type="submit"
                    disabled={isloading}
                    loading={isloading}
                    className="btn btn-primary ml-2"
                    text="Submit"
                  />
                </div>
              </form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
}
