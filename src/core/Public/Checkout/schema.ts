import * as Yup from "yup";

export const checkoutValidation:any=Yup.object().shape({
    id:Yup.string(),
    name:Yup.string().required("Name is required"),
    billing_address:Yup.string().required("Billing Address is required"),
    delivery_address:Yup.string().required("Delivery Address is required"),
    contact:Yup.string().min(10,'Phone number should be of 10 digits').max(10,'Phone number should be of 10 digits').required("Phone Number is required"),
    current_date:Yup.date().required("Please select today's date")
})
