import { Filter } from "components/React/Filter";
import React, { ReactElement, useState } from "react";
import { Link } from "react-router-dom";
import PrivateRoute from "routes/PrivateRoute/PrivateRoute";
import { Card } from "../../../components/React/Card";


import Header from "./Header/Header";
interface Props {
  children: CustomRoute[];
}

export default function Dashboard(props: Props): ReactElement {
  let { children } = props;

  return (
    <>
      <Header />
      <div className="container">
        <PrivateRoute appRoutes={children} />
      </div>
    </>
  );
}
