import React, { useState } from "react";
import { connect } from "react-redux";
import User from "assets/images/user.jpg";
import Logo from "assets/images/info-logo.png";
import { RootStateOrAny, useSelector, useDispatch } from "react-redux";
import {
  decreaseQuantity,
  increaseQuantity,
  deleteFromCart,
} from "store/modules/cart/addToCart";
import ProducImage1 from "assets/images/display-2.jpg";
import ProducImage2 from "assets/images/display-3.jpg";
import { addToCart } from "store/modules/cart/addToCart";

import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";
import { Link } from "react-router-dom";


export const Header = (props) => {
  const { items } = props
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [quantity, setQuantity] = useState(0)
  const toggle = () => setDropdownOpen((prevState) => !prevState);
  const [isOpen, setIsOpen] = useState(false);
  const toggleCollapse = () => setIsOpen(!isOpen);
  const state = useSelector((state: RootStateOrAny) => state.cart)
  console.log('state', state)
  const dispatch = useDispatch()

  const addQuantity = (productId: number, quantity: number, stock: number) => {
    if (quantity < stock) {
      dispatch(increaseQuantity(productId));
    }
  };
  const subQuantity = (productId: number, quantity: number) => {
    if (quantity >= 1) {
      if (quantity > 1) {
        dispatch(decreaseQuantity(productId));
      } else {
        dispatch(deleteFromCart(productId));
      }
    }
  };


const totalAmount = getCommaSeperateNumber(
  state
    .map((item) => Number(item.price.substring(1)) * item.quantity)
    .reduce((acc, current) => {
      return acc + current;
    }, 0)
);
    
  return (
    <>
      <nav className="navbar navbar-sticky">
        <div className="container">
          <div className="navbar-wrapper">
            <div className="toggler d-lg-none d-block">
              <span className="ic-menu" />
            </div>
            <a className="navbar-brand mr-lg-4 mr-0" href="index.html">
              <img src={Logo} height={48} />
              <h5 className="text-white ml-3 mb-0">Shopmandu </h5>
            </a>
            <div className="d-flex align-items-center ml-lg-auto">
              <ul className="list list-inline mr-5 nav-left">
                <li>
                  <a href="#">Home</a>
                </li>

                <li>
                  <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                    <DropdownToggle
                      color="default"
                      className="px-0 cursor-pointer"
                      tag="div"
                    >
                      <div className="nav-link">
                        <i className="ic-shopping-cart" />
                        <span className="badge-pill badge-primary badge-count">
                          {/* {state?.length? state?.length:0} */}
                          {state?.length || 0}
                        </span>
                      </div>
                    </DropdownToggle>
                    <DropdownMenu right>
                      <div className="notification-wrapper">
                        <div className="align-vertical justify-content-between px-3 py-2 ">
                          <h6 className="font-14 mb-0 ">Items added</h6>
                        </div>

                        <DropdownItem divider />
                        
                        <DropdownItem>
                          {state.map((cart,index) => (

                          <div className="align-vertical">
                            <div className="img-sm">
                              <img src={process.env.REACT_APP_IMAGE_API_ENDPOINT + cart?.image} />
                            </div>
                            <div className="flex-grow-1">
                              <p className="font-16 font-weight-medium">
                                {cart?.name}
                              </p>
                              <p className="font-14 mt-1 text-success"> Rs.&nbsp;{getCommaSeperateNumber(cart?.price.substring(1))}
                              </p>
                            </div>
                            <div className="text-center">
                              <p className="   font-weight-medium text-success font-12 mb-2">
                                item: {cart?.stock}
                              </p>

                              <div className="input-group input-group-sm input--quantity_sm">
                                <div className="input-group-prepend">
                                  <button
                                    className="btn btn-outline-gray  border-0"
                                    id="decrease"
                                    value="Decrease Value" onClick={()=>subQuantity(cart?.id, cart?.quantity)}
                                  >
                                    <i className="ic-subtract" />
                                  </button>
                                </div>
                                <input
                                  className="form-control border-0"
                                  type="number"
                                  id="number"
                                  defaultValue={cart?.quantity}
                                />
                                <div className="input-group-append">
                                  <button
                                    className="btn btn-outline-gray  border-0"
                                    id="increase"
                                    value="Increase Value" onClick={()=>addQuantity(cart?.id, cart?.quantity, cart?.stock)}
                                  >
                                    <i className="ic-add" />
                                  </button>
                                </div>
                              </div>
                            </div>
                            
                          </div>
                          ))}
                        </DropdownItem>
                       
                        <DropdownItem divider />
                        
                        <div className="text-right px-3">
                        <p> Total Amount: Rs. {totalAmount}</p>                       
                          <Link to="/checkout">
                          <button className="btn btn-success btn-sm" disabled={state.length === 0}> Checkout</button>
                          </Link>
                          
                        </div>
                      </div>
                    </DropdownMenu>
                  </Dropdown>
                </li>
                <li>
                  <div className="dropdown dropdown-profile">
                    <a
                      className="nav-link avatar p-0 ml-3"
                      href="javascript:void(0)"
                      id="profileDrop"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <img className="img-contain" src={User} />
                    </a>
                    <div
                      className="dropdown-menu dropdown-menu-right"
                      aria-labelledby="profileDrop"
                    >
                      <a className="dropdown-item">
                        {" "}
                        <p className="mb-1">Radindra K.C</p>
                        <p> Radindra.abc@gmail.com</p>
                      </a>
                      <a className="dropdown-item" href="login.html">
                        Login or Register
                      </a>
                      <a className="dropdown-item" href="#">
                        Shipping Address
                      </a>
                      <a className="dropdown-item" href="#">
                        Payment Options
                      </a>
                      <a className="dropdown-item" href="#">
                        Orders
                      </a>
                      <a className="dropdown-item" href="#">
                        Reviews
                      </a>
                      <a className="dropdown-item" href="#">
                        Wishlist
                      </a>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
                          }
const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
