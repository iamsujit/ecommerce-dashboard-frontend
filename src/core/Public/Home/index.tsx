import { Filter } from "components/React/Filter";
import React from "react";
import { connect } from "react-redux";
import { Card } from "../../../components/React/Card";

export const Home = (props) => {
  return (
    <section className="section--spacer">
      <div className="container">
        <div className="section--header">
          <h3 className="  mb-3 text-black">Products</h3>
          {/* <Filter /> */}
        </div>
        <Card />
      </div>
    </section>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
