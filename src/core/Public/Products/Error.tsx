import React from 'react'

export default function Error() {
  return (
    <section className="section--spacer">
      <div className="message message--primary">
        <div className="icon mb-3"><i className="ic-shopping-cart" /></div>
        <p className="mb-4 text-gray-500">There is no item matches your filter</p><a className="btn btn-primary" href="index.html">Continue Shopping</a>
      </div>
    </section>
  )
}
