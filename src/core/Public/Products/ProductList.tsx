import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { getAllProduct } from 'store/modules/products/getProducts'

const ProductList = () => {
    const [products, setProducts] = useState([])
    console.log(products)
    const dispatch = useDispatch()
    const fetchProduct = async () => {
        try {
            const res = await dispatch(getAllProduct())
            setProducts(res.data.product)
            // console.log("ss",res.data)
        } catch (error) {
            
        }
    }
    function addToCartHandler(data){
        alert(`Product Added to: ${data}`)
    }
   useEffect(() => {
       fetchProduct()
   },[])
  return (
    <div>
      {products.map((product:any) => {
          return(
              <>
              <p key={product.id}>{product.name}</p>
              <button onClick={() => addToCartHandler(product.name)}>Add to Cart</button>
              
              </>
              )

      })}
    </div>
  )
}

export default ProductList