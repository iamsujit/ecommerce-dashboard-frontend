import { spawn } from "child_process";
import { Filter } from "components/React/Filter";
import React, { useState, useEffect } from "react";
import { useDispatch } from 'react-redux'
import { connect } from "react-redux";
import { getAllProduct } from "store/modules/products/getProducts";
import { Card } from "../../../components/React/Card";
import Error from "./Error";

export const Home = (props) => {
  
  const [products, setProducts] = useState<any>([])
  const [filterData, setFilterData] = useState<any>([]);
  React.useState<{
    id: string,
    name: string,
    is_folder: boolean,
    file: string,
    parent: string,
  } | null>(null)
  const [filteredList, setFilteredList] = useState<any>([]);

  const dispatch = useDispatch()
  const fetchProduct = async () => {
    try {
      const res = await dispatch(getAllProduct())
      setProducts(res.data?.product || [])
    } catch (error) {

    }
  }
  function addToCartHandler(data) {
    alert(`Product Added to: ${data}`)
  }

  useEffect(() => {
    fetchProduct()
  }, [])


  return (
    <section className="section--spacer">
      <div className="section--header mb-3">
        <h3 className="text-black">Products</h3>
        {products && (
          <Filter items={products} setFilterData={setFilterData} filterData={filterData} filteredList={filteredList} setFilteredList={setFilteredList} />
        )}

      </div>
      {filterData && (
        <div className="mb-3">
          {filterData.category && filterData.category.map((filter) => (<span className="badge badge-md badge-pill badge-primary mr-1">
            Category: {filter}
          </span>))}
          {filterData.priceMin && (
            <span className="badge badge-pill badge-md badge-primary mr-1">
              Min Price: {filterData.priceMin}
            </span>
          )}
          {filterData.priceMax && (
            <span className="badge badge-pill badge-md badge-primary mr-1">
              Max Price: {filterData.priceMax}
            </span>)}

          {filterData.date && (<span className="badge badge-pill badge-md badge-primary mr-1">Date:{filterData.date}</span>)}
        </div>
      )}

      <Card items={filteredList && filteredList?.length > 0 ? filteredList : products || []}  />
    </section>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Home);