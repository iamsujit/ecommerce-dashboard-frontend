import { lazy } from "react";

const Dashboard = lazy(() => import("core/Public/Dashboard/Dashboard"));
const Checkout = lazy(() => import("core/Public/Checkout/Checkout"));
const Home = lazy(() => import("core/Public/Products/ProductListIntegration"));
const CheckoutForm = lazy(() => import("core/Public/Checkout/CheckoutForm"));





export const appRoutes: CustomRoute[] = [
  {
    path: "/",
    component: Dashboard,
    type: "unauthorized",
    children: [
      {
        path: "/",
        component: Home,
        type: "unauthorized",
        exact: true,
      },

      {
        path: "/checkout",
        component: Checkout,
        type: "unauthorized",
        exact: true,
      },
      {
        path: "/checkout-form",
        component: CheckoutForm,
        type: "unauthorized",
        exact: true,
      },
    ],
  },
];
