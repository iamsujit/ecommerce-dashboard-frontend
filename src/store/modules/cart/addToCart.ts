export const ADD_TO_CART = 'ADD_TO_CART';
export const DELETE_FROM_CART = 'DELETE_FROM_CART'
export const INCREASE_QUANTITY = 'INCREASE_QUANTITY'
export const  DECREASE_QUANTITY = ' DECREASE_QUANTITY'
export const EMPTY_CARD = 'EMPTY_CART'



export const addToCart = (product: any): DefaultAction => {
    return {
        type: 'ADD_TO_CART',
        payload: product,

    }
}
export const deleteFromCart = (product: any): DefaultAction => {
    return {
        type: 'Delete_FROM_CART',
        payload: product,

    }
}

export const increaseQuantity = (product: any): DefaultAction => {
    return {
      type: INCREASE_QUANTITY,
      payload: product,
    }
  }
  
  export const decreaseQuantity = (product: any): DefaultAction => {
    return {
      type: DECREASE_QUANTITY,
      payload: product,
    }
  }

export const emptyCart = () => {
    return{
        type: 'EMPTY_CART',
        payload: null
    }
}

    
export type cartState = {
    id: number;
    name: string;
    price: string;
    image: string;
    stock: number;
    createDate: string;
    quantity: number;
    category: string[];
}[];

const cart: cartState = [];

const cartReducer = (state = cart, action: DefaultAction) => {
    const product = action.payload;
    switch (action.type) {
        case ADD_TO_CART:
            if (
                state.filter((object) => object.id === action.payload.id).length !== 0
            ) {
                let indexIncrease = state.findIndex((object) => object.id === action.payload.id);
                let productToIncrease = [...state];
                let increaseItem = { ...productToIncrease[indexIncrease] };
                if (increaseItem.stock > increaseItem.quantity) {
                    increaseItem = { ...increaseItem, quantity: increaseItem.quantity + 1 };
                }
                productToIncrease[indexIncrease] = increaseItem;
                return [...productToIncrease];
            }
            return [...state, action.payload];

        case DELETE_FROM_CART:
            let deleted = state.filter((product) => product.id !== action.payload);
            return [...deleted];

        case INCREASE_QUANTITY:
            let indexIncrease = state.findIndex((obj) => obj.id === action.payload);
            let productToIncrease = [...state];
            let increaseItem = { ...productToIncrease[indexIncrease] };
            increaseItem = { ...increaseItem, quantity: increaseItem.quantity + 1 };
            productToIncrease[indexIncrease] = increaseItem;
            return [...productToIncrease];

        case DECREASE_QUANTITY:
            let indexDecrease = state.findIndex((obj) => obj.id === action.payload);
            let productToDecrease = [...state];
            let decreaseItem = { ...productToDecrease[indexDecrease] };
            decreaseItem = { ...decreaseItem, quantity: decreaseItem.quantity - 1 };
            productToDecrease[indexDecrease] = decreaseItem;
            return [...productToDecrease];
        case EMPTY_CARD:
                return product
        default:
            return state;

        
    }
}

export default cartReducer;

