import { apiList } from "store/actionNames";
import initDefaultAction from "store/helper/default-action";
import initDefaultReducer from "store/helper/default-reducer";
import initialState from "store/helper/default-state";



const obj = initialState;
let apiDetails = apiList.products.getProducts;

export default function getAllProductReducer(store = { ...obj }, action) {
  let state = Object.assign({}, store);
  let actionName = apiDetails.actionName;
  return initDefaultReducer(actionName, action, state)
}

export const getAllProduct = (): any => async (dispatch) => {
  const resData = await initDefaultAction(apiDetails, dispatch)
  return resData;
}
