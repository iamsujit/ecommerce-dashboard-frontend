import { AnyAction, combineReducers } from "redux";

import i18nextReducer from "./modules/i18n/i18n";
import TokenService from "../services/jwt-token/jwt-token";
import externalServiceReducer from "./modules/externalService";
import cartReducer from "./modules/cart/addToCart";


export const appReducer = combineReducers({
    cart: cartReducer,
    i18nextData: i18nextReducer,
    externalServiceData: externalServiceReducer,
});

export type RootState = ReturnType<typeof appReducer>;
type TState = ReturnType<typeof appReducer> | undefined;

export default function rootReducer(state: TState, action: AnyAction) {
    if (action.type === "USER_LOG_OUT") {
        state = undefined;
        try {
        } catch (err) {
            console.error("Logout Error", err);
        }
    }

    return appReducer(state, action);
};

export const logoutAction = () => {
    try {
        TokenService.clearToken();
    } catch (err) {
        console.error("LogOut Error", err);
    }

    return { type: "USER_LOG_OUT", payload: {} };
};
